import React from "react";
import Photo from "../Images/photo.png"

const Home = () => {
return (
    <div className="container-fit m-3 container-body">
	<div className="row justify-content-center align-items-center">
		<div className="col-md-4 order-1 order-md-2">
			<img src={Photo} className="img-fluid rounded-circle my-3 p-3"/>
		</div>
		<div class="col-md-6 order-2 order-md-1 text-white text-center my-2">
			<h1 className="mb-4">Donald Morales</h1>
			<h2 className="mb-4">Full Stack Web Developer</h2>
			<p className="mb-4">Turning anyone's idea into digital reality using the modern tools and technology today.</p>
		</div>
	</div>
	</div>
)
}

export default Home
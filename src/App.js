import React from 'react';
import Home from "./Home/Home"
import Nav from "./Nav/Nav"
import Footer from "./Footer/Footer"
import Expertise from "./Expertise/Expertise"
import Project from "./Project/Project"
import Tool from "./Tool/Tool"
import Contact from "./Contact/Contact"

function App() {
  return (
    <div>
    <Nav />
    <Home />
    <Expertise />
    <Project />
    <Tool />
    <Contact />

    <Footer />
    
    </div>
  );
}

export default App;
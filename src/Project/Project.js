import React from "react";
import Finances from "../Images/finances.PNG";
import Pix from "../Images/pix.PNG" 


const Project = () => {
return (
    <div className="mt-5">
    <h2 class="text-center text-white mb-4">My Sample Projects</h2>
<h4 class="text-center text-white mb-4">Click image to visit the website</h4>
<h6 class="text-center text-white">password: mm username: a</h6>
<div class="container-fluid" id="project">
	<div class="row my-3 pt-3">
		<div class="col-md-6 mx-auto">
<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <a href="https://track-your-finances.netlify.app/" target="_blank"><img src={Finances} class="d-block w-100 img-fluid mt-5" alt="..." /></a>
      
        
      
    </div>
    <div class="carousel-item">
      <a href="https://top10-tourist-spots-in-the-philippines.netlify.app/" target="_blank"><img src={Pix} class="d-block w-100 img-fluid mt-5" alt="..." /></a>
      
    </div>
    
    
    </div>
  </div>
  <button class=" mb-5 carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </button>
  <button class="mb-5 carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </button>
</div>

</div>
</div>
</div>

)
}


export default Project
import React from "react";

const Nav = () => {
return (
    <nav className="navbar fixed-top bg-dark navbar-expand-lg navbar-dark mb-5">
			<a className="navbar-brand d-md-none" href="./index.html">DONALD</a>
 	<button className="navbar-toggler border border-white" type="button" data-toggle="collapse" data-target="#sample-nav">
 		<span className="navbar-toggler-icon"></span>
 	</button>
 	<div className="collapse navbar-collapse" id="sample-nav">
 		<ul className="navbar-nav mx-auto">
 			<li className="nav-item">
 			<a href="./index.html" className="nav-link text-white">HOME</a>
 		</li>
 			<li>
 				<a href="./projects.html" className="nav-link text-white">PROJECTS</a>
 			</li>
 			<li>
 				<a href="./tools.html" className="nav-link text-white">TOOLS</a>
 			</li>
 			<li>
 				<a href="./contacts.html" className="nav-link text-white">CONTACT</a>
 			</li>
 		</ul>
 	</div>
</nav>
)
}

export default Nav;
import React from "react";


const Expertise = () => {
    return (
        
        <div>
        <h1 className="text-center pt-31 mt-3 text-white">Expertise</h1>
        <div class="container-fit m-3 p-2 bg-white" id="expertise">
        
	<div className="row justify-content-center align-items-center">
    
		<div class="col-md-4">
			<img src="https://images.pexels.com/photos/38568/apple-imac-ipad-workplace-38568.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-fluid" />
			<h3 class="text-center pt-3">Web Application</h3>	
		</div>
		<div class="col-md-4">
			<img src="https://images.pexels.com/photos/5082579/pexels-photo-5082579.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" className="img-fluid" />
			<h3 className="text-center pt-3">Mobile Application</h3>
		</div>
        <div class="col-md-4">
			<img src="https://media.istockphoto.com/photos/application-programming-interface-middleware-for-applications-to-picture-id1167364401?k=20&m=1167364401&s=612x612&w=0&h=cZ6tTFiTsD_VSd-MCIc1EYPg87SyI0lK_RKKI8DoN6Q=" class="img-fluid" />
			<h3 class="text-center pt-3">Middleware</h3>	
		</div>
	</div>
    </div>
    </div>
    );
}

export default Expertise;
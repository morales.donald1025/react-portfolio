import React from "react";

import Node from "../Images/nodejs.png"
import Mongo from "../Images/mongodb.png"
import LogoReact from "../Images/logo-react.png"
import Express from "../Images/logo-expressjs.png"
import JS from "../Images/js.png"
import Git from "../Images/logo-git.png"
import Sublime from "../Images/logo-sublime-text-3.png"
import Postman from "../Images/logo-postman.png"
import Gitlab from "../Images/logo-gitlab-ci-cd.png"
import HTML from "../Images/logo-html5.png"
import Bootstrap from "../Images/logo-bootstrap.png"
import CSS from "../Images/logo-css3.png"












const Tool = () => {
return (
<div>
    <h2 class="text-center text-white">Technology And Tools</h2>
<div class="container-fit m-4 bg-white align-items-center" id="tools">

	<div class="row">
	<div class="justify-content-center pt-3 mt-3 col-6 col-md-2">
	<img src={Node} class="img-fluid" />
</div>
<div class="justify-content-center mb-2 pt-2 col-6 col-md-2">
	<img src={Mongo} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={LogoReact} class="img-fluid" />
</div>
<div class="align-items-center col-6 my-2 col-md-2">
	<img src={Express} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={JS} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={Git} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={Sublime} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={Postman} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={Gitlab} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={HTML} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={Bootstrap} class="img-fluid" />
</div>
<div class="justify-content-center my-2 col-6 col-md-2">
	<img src={CSS} class="img-fluid" />
</div>
</div>
	</div>
    </div>

)
}

export default Tool;
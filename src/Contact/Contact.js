import React from "react";
import "./Contact.css"


const Contact = () => {
    return (
        <div class="container-fluid my-5">
	<div class="row align-items-center text-white">
		<div class="col-md-6">
			<h2 class="text-center">Find Us</h2>
			<iframe id="gmap_canvas" src="https://maps.google.com/maps?q=manila&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
		</div>
		<div class="col-md-6 my-4">
			<h2 class="text-center">Contact Us</h2>
			<form>
				<div class="form-group">
				<label for="text-name">Full Name:</label>
				<input type="text" name="name" id="text-name" class="form-control" />
			</div>

			<div class="form-group">
				<label for="text-email">Email:</label>
				<input type="email" name="email" id="text-email" class="form-control" />
			</div>
			
			<div class="form-group">
				<label for="text-textarea">Message</label>
				<textarea class="form-control" id="text-textarea" rows="3"></textarea>
			</div>
			<button type="submit" class="btn btn-outline-success btn-block text-white">Submit
			</button>
			<button type="reset" class="btn btn-outline-danger btn-block text-white">
			Reset
		</button>
		</form>
		</div>
	</div>
</div>
    )
}

export default Contact;